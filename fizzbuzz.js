function fizzbuzzz(maxValue) {
    let buzziest = "";

    for(let i = 1; i < maxValue + 1; i++) 
        if (i % 2 === 0 && i % 3 === 0) buzziest += "FizzBuzz,"
        else if (i % 2 === 0) buzziest += "Fizz,"
        else if (i % 3 === 0) buzziest += "Buzz,"
        else buzziest += `${i},`
        
    return buzziest;
}